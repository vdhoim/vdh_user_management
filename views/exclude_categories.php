<form method="post" action="options.php">
    <?php settings_fields('exclude_categories'); ?>
    <?php do_settings_sections('exclude_categories'); ?>
    <div class="wrap">
        <h2>User Management Plugin settings</h2>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Exclude Categories</th>
                <td>
                    <select name="exclude_categories[]" multiple="multiple"> 
                        <?php foreach ($all_categories as $category): ?>
                            <option value="<?php echo $category->cat_ID ?>"
                            <?php echo (in_array($category->cat_ID, $exclude_categories) ? 'selected="selected"' : '') ?>
                                    ><?php echo $category->cat_name ?></option>
                                <?php endforeach; ?>
                    </select>
                </td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </div>
</form>