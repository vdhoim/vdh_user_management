<table class="form-table">
    <tbody>
        <tr>
            <th><label for="departments">Assign User to Department(s)</label></th>
            <td>
                <select name="departments[]" multiple="multiple" id="departments" class="regular-text ltr" style="width:15em">
                    <?php foreach ($departments as $department): ?>
                        <option value="<?= $department->term_id ?>"<?= ( in_array($department->term_id, $user_departments) ? ' selected="selected"' : ''); ?>><?= $department->name ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
    </tbody>
</table>