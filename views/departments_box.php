<div id="taxonomy-<?php echo $tax_name; ?>" class="categorydiv">
    <ul id="<?php echo $tax_name; ?>-tabs" class="category-tabs">
        <li class="tabs"><a href="#<?php echo $tax_name; ?>-all"><?php _e('Departments'); ?></a></li>
        <li class="hide-if-no-js"><a href="#<?php echo $tax_name; ?>-pop"><?php _e('Recently posted for'); ?></a></li>
    </ul>

    <div id="<?php echo $tax_name; ?>-pop" class="tabs-panel" style="display: none;">
        <ul id="<?php echo $tax_name; ?>checklist-pop" class="categorychecklist form-no-clear" >
            <?php wp_terms_checklist($post->ID, array('taxonomy' => $tax_name, 'walker' => $popular_cats_walker)); ?>
        </ul>
    </div>

    <div id="<?php echo $tax_name; ?>-all" class="tabs-panel">
        <?php
        $name = ( $tax_name == 'category' ) ? 'post_category' : 'tax_input[' . $tax_name . ']';
        echo "<input type='hidden' name='{$name}[]' value='0' />"; // Allows for an empty term set to be sent. 0 is an invalid Term ID and will be ignored by empty() checks.
        ?>
        <ul id="<?php echo $tax_name; ?>checklist" data-wp-lists="list:<?php echo $tax_name; ?>" class="categorychecklist form-no-clear">
            <?php wp_terms_checklist($post->ID, array('taxonomy' => $tax_name, 'walker' => $walker)); ?>
        </ul>
    </div>
    <?php if (current_user_can($taxonomy->cap->edit_terms)) : ?>
        <div id="<?php echo $tax_name; ?>-adder" class="wp-hidden-children">
            <h4>
                <a id="<?php echo $tax_name; ?>-add-toggle" href="#<?php echo $tax_name; ?>-add" class="hide-if-no-js">
                    <?php
                    /* translators: %s: add new taxonomy label */
                    printf(__('+ %s'), $taxonomy->labels->add_new_item);
                    ?>
                </a>
            </h4>
            <p id="<?php echo $tax_name; ?>-add" class="category-add wp-hidden-child">
                <label class="screen-reader-text" for="new<?php echo $tax_name; ?>"><?php echo $taxonomy->labels->add_new_item; ?></label>
                <input type="text" name="new<?php echo $tax_name; ?>" id="new<?php echo $tax_name; ?>" class="form-required form-input-tip" value="<?php echo esc_attr($taxonomy->labels->new_item_name); ?>" aria-required="true"/>
                <label class="screen-reader-text" for="new<?php echo $tax_name; ?>_parent">
                    <?php echo $taxonomy->labels->parent_item_colon; ?>
                </label>
                <?php wp_dropdown_categories(array('taxonomy' => $tax_name, 'hide_empty' => 0, 'name' => 'new' . $tax_name . '_parent', 'orderby' => 'name', 'hierarchical' => 1, 'show_option_none' => '&mdash; ' . $taxonomy->labels->parent_item . ' &mdash;')); ?>
                <input type="button" id="<?php echo $tax_name; ?>-add-submit" data-wp-lists="add:<?php echo $tax_name; ?>checklist:<?php echo $tax_name; ?>-add" class="button category-add-submit" value="<?php echo esc_attr($taxonomy->labels->add_new_item); ?>" />
                <?php wp_nonce_field('add-' . $tax_name, '_ajax_nonce-add-' . $tax_name, false); ?>
                <span id="<?php echo $tax_name; ?>-ajax-response"></span>
            </p>
        </div>
    <?php endif; ?>
</div>