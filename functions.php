<?php

class User_Management_Functions {

    public function admin_init() {
        if (!current_user_can('administrator')) {
            remove_menu_page('tools.php');
        }
        //register settings for the plugin
        register_setting('exclude_categories', 'exclude_categories');
    }

    public function settings_page() {
        add_options_page('User Management Plugin options', 'User Management', 'manage_options', 'user-management', array($this, 'display_settings_page'));
    }

    public function display_settings_page() {
        $this->render_view('exclude_categories', array(
            'exclude_categories' => get_option('exclude_categories') ? get_option('exclude_categories') : array(),
            'all_categories' => $this->get_departments()
        ));
    }

    public function settings_link($links) {
        if (is_network_admin()) {
            $u = 'settings.php';
        } else {
            $u = 'options-general.php';
        }
        $settings_link = "<a href=\"$u?page=user-management\">Settings</a>";
        array_unshift($links, $settings_link);
        return $links;
    }

    /**
     * Display post categories form fields.
     *
     * @since 2.6.0
     *
     * @todo Create taxonomy-agnostic wrapper for this.
     *
     * @param WP_Post $post Post object.
     * @param array   $box {
     *     Categories meta box arguments.
     *
     *     @type string   $id       Meta box ID.
     *     @type string   $title    Meta box title.
     *     @type callback $callback Meta box display callback.
     *     @type array    $args {
     *         Extra meta box arguments.
     *
     *         @type string $taxonomy Taxonomy. Default 'category'.
     *     }
     * }
     */
    public function custom_post_categories_meta_box($post, $box) {

        $defaults = array('taxonomy' => 'category');
        if (!isset($box['args']) || !is_array($box['args'])) {
            $args = array();
        } else {
            $args = $box['args'];
        }
        $r = wp_parse_args($args, $defaults);
        $tax_name = esc_attr($r['taxonomy']);
        $taxonomy = get_taxonomy($r['taxonomy']);

        $departments = $this->get_user_departments(get_current_user_id());
        $popular_ids = array_intersect($departments, wp_popular_terms_checklist($tax_name, 0, 10, false));

        $this->render_view('departments_box', array(
            'r' => $r,
            'tax_name' => $tax_name,
            'taxonomy' => $taxonomy,
            'post' => $post,
            'walker' => new Walker_Departments($departments),
            'popular_cats_walker' => new Walker_Departments($popular_ids)
        ));
    }

    public function modify_user_columns($column_headers) {
        unset($column_headers['name']);

        $new_column_headers = array_slice($column_headers, 0, 4, true) +
                array("departments" => "Department(s)") +
                array_slice($column_headers, 4, count($column_headers) - 4, true);

        return $new_column_headers;
    }

    // Remove 'Administrator' from the list of roles if the current user is not an admin
    public function modify_editable_roles($roles) {
        if (isset($roles['administrator']) && !current_user_can('administrator')) {
            unset($roles['administrator']);
        }
        return $roles;
    }

    private function get_user_departments($user_id) {
        $user_departments = get_the_author_meta('departments', $user_id);
        return is_array($user_departments) ? $user_departments : array();
    }

    private function is_visible_to_user($editor_id, $author_id) {
        return current_user_can('administrator') || (bool) array_intersect(
                        $this->get_user_departments($editor_id), $this->get_user_departments($author_id));
    }

    public function map_meta_cap($caps, $cap, $user_id, $args) {

        //let editor manage users
        $editor = get_role('editor');

        if (!$editor->has_cap('list_users')) {
            $editor->add_cap('list_users');
        }
        if (!$editor->has_cap('create_users')) {
            $editor->add_cap('create_users');
        }
        if (!$editor->has_cap('edit_users')) {
            $editor->add_cap('edit_users');
        }
        if (!$editor->has_cap('delete_users')) {
            $editor->add_cap('delete_users');
        }

        //add some rules
        switch ($cap) {
            case 'edit_user':
            case 'remove_user':
            case 'promote_user':
            case 'delete_user':
            case 'delete_users':
                if (isset($args[0]) && !$this->is_visible_to_user($user_id, absint($args[0]))) {
                    $caps[] = 'do_not_allow';
                }
                break;
        }
        return $caps;
    }

    public function modify_user_table_row($uno, $dos, $user_id) {
        $departments = '';
        $user_depts = $this->get_user_departments($user_id);
        $depts = get_categories(array('hide_empty' => 0));
        foreach ($depts as $dept) {
            if (in_array($dept->term_id, $user_depts)) {
                $departments .= "{$dept->name} <br />";
            }
        }
        return $departments;
    }

    public function filter_users($query) {

        if (current_user_can('administrator')) {
            return;
        }

        $departments = $this->get_user_departments(get_current_user_id());
        $meta_query = array('relation' => 'OR');

        foreach ($departments as $department) {
            $meta_query[] = array(
                'key' => 'departments',
                'value' => ';' . serialize((string) $department),
                'compare' => 'LIKE'
            );
        }

        $query->set('meta_query', $meta_query);

        return;
    }

    public function pre_user_query($query) {
        if (isset($query->query_vars['role']) && $query->query_vars['role'] && !current_user_can('administrator')) {
            $lines = array();
            preg_match_all('/^[WHERE|OR].*$/im', $query->query_where, $lines);
            $lines[0][count($lines[0]) - 1] = preg_replace('/^OR/', 'AND', $lines[0][count($lines[0]) - 1]);
            $lines[0][count($lines[0]) - 1] = substr($lines[0][count($lines[0]) - 1], 0, -1);
            $lines[0][count($lines[0]) - 2] .= ')';
            $query->query_where = implode("\n", $lines[0]);
        }
    }

    public function remove_default_categories_box() {
        // Only proceed if user does not have admin role.
        if (!current_user_can('manage_options')) {
            remove_meta_box('submitdiv', 'post', 'normal'); // Publish meta box
            remove_meta_box('commentsdiv', 'post', 'normal'); // Comments meta box
            remove_meta_box('revisionsdiv', 'post', 'normal'); // Revisions meta box
            remove_meta_box('authordiv', 'post', 'normal'); // Author meta box
            remove_meta_box('slugdiv', 'post', 'normal'); // Slug meta box
            remove_meta_box('tagsdiv-post_tag', 'post', 'side'); // Post tags meta box
            remove_meta_box('categorydiv', 'post', 'side'); // Category meta box
            remove_meta_box('postexcerpt', 'post', 'normal'); // Excerpt meta box
            remove_meta_box('formatdiv', 'post', 'normal'); // Post format meta box
            remove_meta_box('trackbacksdiv', 'post', 'normal'); // Trackbacks meta box
            remove_meta_box('postcustom', 'post', 'normal'); // Custom fields meta box
            remove_meta_box('commentstatusdiv', 'post', 'normal'); // Comment status meta box
            remove_meta_box('postimagediv', 'post', 'side'); // Featured image meta box	
            remove_meta_box('pageparentdiv', 'page', 'side'); // Page attributes meta box
        }
    }

    public function add_custom_categories_box() {
        // Only proceed if user does not have admin role.
        if (!current_user_can('administrator')) {
            add_meta_box('customcategorydiv', 'Departments', array($this, 'custom_post_categories_meta_box'), 'post', 'side', 'low', array('taxonomy' => 'category'));
        }
    }

    public function add_departments($user) {
        if (current_user_can('edit_users')) {
            $user_departments = array();
            if (is_a($user, 'WP_User')) {
                $user_departments = $this->get_user_departments($user->ID);
            }
            $departments = $this->filter_departments($this->get_available_departments());
            $this->render_view('departments', array('departments' => $departments, 'user_departments' => $user_departments));
        }
    }

    public function filter_categories($terms, $taxonomies, $args) {

        if (!current_user_can('administrator') && in_array('category', $taxonomies) && $args['child_of'] === 0) {
            $user_departments = $this->get_available_departments();

            foreach ($user_departments as $user_department) {
                foreach (get_terms('category', array('child_of' => (int) $user_department, 'hide_empty' => false)) as $child_category) {
                    if (!in_array($child_category->term_id, $user_departments)) {
                        $user_departments[] = $child_category->term_id;
                    }
                }
            }

            foreach ($terms as $key => $term) {
                if (!in_array($term->term_id, $user_departments)) {
                    unset($terms[$key]);
                }
            }
        }
        return $terms;
    }

    public function remove_updates() {
        global $wp_version;
        return(object) array('last_checked' => time(), 'version_checked' => $wp_version);
        //add_action('init', create_function('$a',"remove_action( 'init', 'wp_version_check' );"), 2);
        //add_filter('pre_option_update_core','__return_null');
        //add_filter('pre_site_transient_update_core','__return_null');
    }

    public function admin_footer_text($text) {
        //modify $text if needed or modify return statement to show the desired information in footer
        return '';
    }

    public function admin_footer_version_text($text) {
        return '';
    }

    private function filter_departments(array $available_departments = array()) {
        $all_departments = $this->get_departments();
        $not_departments = get_option('exclude_categories') ? get_option('exclude_categories') : array();
        $departments = array();
        foreach ($all_departments as $department) {
            if (in_array($department->term_id, array_diff($available_departments, $not_departments))) {
                $departments[] = $department;
            }
        }
        return $departments;
    }

    private function get_departments() {
        $departments = array();
        $all_departments = get_categories(array('hide_empty' => false));
        foreach ($all_departments as $department) {
            if ($department->category_parent === '0') {
                $departments[] = $department;
            }
        }
        return $departments;
    }

    private function get_available_departments() {
        if (current_user_can('administrator')) {
            $departments = array();
            $depts = get_categories(array('hide_empty' => false));
            foreach ($depts as $dept) {
                $departments[] = $dept->term_id;
            }
            return $departments;
        }
        return $this->get_user_departments(get_current_user_id());
    }

    public function save_departments($user_id) {
        if (current_user_can('edit_users')) {

            $editor_departments = $this->get_available_departments();
            $user_departments = array_diff($this->get_user_departments($user_id), $editor_departments);
            $post_departments = array_intersect($_POST['departments'], $editor_departments);

            $departments = array();
            foreach (array_merge($user_departments, $post_departments) as $department) {
                $departments[] = sanitize_text_field($department);
            }
            update_user_meta($user_id, 'departments', $departments);
        }
    }

    public function remove_user_count($users) {
        unset($users['administrator']);
        foreach ($users as $key => $user) {
            $user = preg_replace('/\s*\<span[^\>]+\>\(\d+\)\<\/span\>/', 's', $user);
            $users[$key] = preg_replace('/Alls/', 'All', $user);
        }
        return $users;
    }

    /**
     * Adding scripts to our plugin
     */
    public function add_scripts() {
        //add scripts if any. See example below
        //wp_enqueue_script('script.js', plugins_url('/user-managemet/assets/js/script.js'), array(), '1.0.0', true);
        //wp_enqueue_style('style.css', plugins_url('/user-management/assets/css/backend/style.css'));
    }

    /**
     * Disable admin bar on the frontend for subscribers.
     */
    public function disable_admin_bar() {
        if (!current_user_can('edit_posts')) {
            add_filter('show_admin_bar', '__return_false');
        }
    }

    /**
     * Redirect back to homepage and not allow access to 
     * WP admin for Subscribers.
     */
    public function redirect_non_admins() {
        if (!current_user_can('edit_posts')) {
            wp_redirect(site_url());
            exit;
        }
    }

    protected function render_view($view, array $data = array()) {
        if ($data) {
            extract($data);
        }
        include BASE_PATH . '/views/' . $view . '.php';
    }

}

/**
 * 
 * 
 */
class Walker_Departments extends Walker {

    private $_categories;
    // Tell Walker where to inherit it's parent and id values
    public $db_fields = array(
        'parent' => 'parent',
        'id' => 'term_id'
    );

    public function __construct(array $categories) {
        $this->_categories = $categories;
    }

    public function start_el(&$output, $category, $depth = 0, $args = array(), $id = 0) {

        if (empty($args['taxonomy'])) {
            $taxonomy = 'category';
        } else {
            $taxonomy = $args['taxonomy'];
        }

        if ($taxonomy == 'category') {
            $name = 'post_category';
        } else {
            $name = 'tax_input[' . $taxonomy . ']';
        }
        $args['popular_cats'] = empty($args['popular_cats']) ? array() : $args['popular_cats'];
        $class = in_array($category->term_id, $args['popular_cats']) ? ' class="popular-category"' : '';

        $args['selected_cats'] = empty($args['selected_cats']) ? array() : $args['selected_cats'];

        if (in_array($category->term_id, $this->_categories)) {
            $output .= "\n<li id='{$taxonomy}-{$category->term_id}'$class>" .
                    '<label class="selectit"><input value="' . $category->term_id . '" type="checkbox" name="' . $name . '[]" id="in-' . $taxonomy . '-' . $category->term_id . '"' .
                    checked(in_array($category->term_id, $args['selected_cats']), true, false) .
                    disabled(empty($args['disabled']), false, false) . ' /> ' .
                    esc_html(apply_filters('the_category', $category->name)) . '</label>';
        }
    }

}
