<?php

class Admin_User_Management {

    static function getInstance() {
        return new self();
    }

    /**
     * adding actions and hooks
     */
    public function addActions() {

        $um = new User_Management_Functions;

        //remove core updates notification and all kinds of notification
        add_filter('pre_site_transient_update_core', array($um, 'remove_updates'));
        add_filter('pre_site_transient_update_plugins', array($um, 'remove_updates'));
        add_filter('pre_site_transient_update_themes', array($um, 'remove_updates'));

        add_action('admin_init', array($um, 'admin_init'));
        add_action('wp_enqueue_scripts', array($um, 'add_scripts'));

        //adding departments support
        add_action('show_user_profile', array($um, 'add_departments'));
        add_action('edit_user_profile', array($um, 'add_departments'));
        add_action('user_new_form', array($um, 'add_departments'));
        add_action('personal_options_update', array($um, 'save_departments'));
        add_action('edit_user_profile_update', array($um, 'save_departments'));
        add_action('user_register', array($um, 'save_departments'));

        //remove old box
        add_action('admin_head', array($um, 'remove_default_categories_box'));
        //add new box
        add_action('admin_menu', array($um, 'add_custom_categories_box'));

        //adding departments to user table in admin panel
        add_action('manage_users_columns', array($um, 'modify_user_columns'));

        //and filling it with the data
        add_filter('manage_users_custom_column', array($um, 'modify_user_table_row'), 1, 10);

        //filtering the users
        add_filter('pre_get_users', array($um, 'filter_users'), 10, 3);

        //pre user query
        add_filter('pre_user_query', array($um, 'pre_user_query'));

        //removing the user count
        add_filter('views_users', array($um, 'remove_user_count'));

        //removing admins from editable roles (just in case)
        add_filter('editable_roles', array($um, 'modify_editable_roles'));

        //implementing logic on edit users operations
        add_filter('map_meta_cap', array($um, 'map_meta_cap'), 10, 4);

        //filtering categories|departments
        add_filter('get_terms', array($um, 'filter_categories'), 10, 3);

        //removing/modifying "Thank you for creating with WordPress text" and WP version number
        add_filter('admin_footer_text', array($um, 'admin_footer_text'));
        add_filter('update_footer', array($um, 'admin_footer_version_text'), 9999);

        //add settings link
        add_action('admin_menu', array($um, 'settings_page'));
        
        //creating settings link
        if (is_multisite()) {
            add_filter("network_admin_plugin_action_links_" . plugin_basename(BASE_PATH . '/user-management.php'), array($um, 'settings_link'));
        } else {
            add_filter("plugin_action_links_" . plugin_basename(BASE_PATH . '/user-management.php'), array($um, 'settings_link'));
        }
    }

}

Admin_User_Management::getInstance()->addActions();
